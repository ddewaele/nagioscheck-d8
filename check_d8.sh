#!/bin/sh
output=`cd $1 && drush pm:security 2>&1`
if [[ $output == *"no outstanding security updates"* ]]; then
  echo "[OK] No security updates needed"
  exit 0
fi

if [[ $output == *"has an outstanding security update"* ]]; then
  echo "[CRITICAL] Security update available"
  exit 2
fi

exit 2
